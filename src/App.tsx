import { Data, FormMatkulku } from './Components/FormMatkul';
import { Navbar } from './Components/Navbar';
import { Card } from './Components/Card';
import React from 'react';

import { AppElement, ListMatkul } from './Components/StyledComponents';
import { Footer } from './Components/Footer';

interface AppState {
  data: Data[];
}

class App extends React.Component<any, AppState> {
  constructor(props: any) {
    super(props);
    this.state = { data: [] };
    this.onSubmitMatkul = this.onSubmitMatkul.bind(this);
    this.handleDeletCard = this.handleDeletCard.bind(this);
  }

  onSubmitMatkul(freshData: Data): void {
    this.setState({
      data: [...this.state.data, freshData],
    });
  };

  handleDeletCard(idCard: string): void {
    const filteredData = this.state.data.filter((d: Data) => d.id !== idCard);
    this.setState({
      data: [...filteredData],
    });
  };

  render() {
    return (
      <>
        <Navbar />
        <AppElement>
          <FormMatkulku getData={this.onSubmitMatkul} />
          <ListMatkul>
            {this.state.data.map((d: Data, index: number) => (
              <Card
                title={d.matkul + ' ( ' + d.sks + ' SKS )'}
                dosen={d.dosen}
                deskripsi={d.deskripsi}
                semester={d.semester + ' ' + d.tahun}
                key={d.id}
                id={d.id ? d.id : 'xyz' + index}
                onDelete={this.handleDeletCard}
              />
            ))}
          </ListMatkul>
        </AppElement>
        <Footer />
      </>
    );
  }
}

export default App;
