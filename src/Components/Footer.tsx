import { FC } from 'react';
import { FooterContainer } from './StyledComponents';

export const Footer: FC = (): JSX.Element => {
  return (
    <FooterContainer>
      <p>&#60;&#47;&#62; with ❤️️ by Taufik Pragusga</p>
    </FooterContainer>
  );
};
