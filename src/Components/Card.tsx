import { useForm } from 'react-hook-form';
import React, { FC, useState } from 'react';

import {
  CardForm,
  CardLabel,
  CardTitle,
  CardInput,
  CardBottom,
  CardButton,
  CardContent,
  CardContainer,
  CardLeftContent,
  CardRightContent,
  CardTitleAddTugas,
  CardInputContainer,
  CardButtonContainer,
  CardContentContainer,
} from './StyledComponents';

interface CardProps {
  title: string;
  dosen: string;
  deskripsi: string;
  semester: string;
  id: string;
  onDelete: (idCard: string) => void;
}

type Tugas = { tugas: string; deadline: string };

export const Card: FC<CardProps> = ({
  id,
  title,
  dosen,
  semester,
  onDelete,
  deskripsi,
}): JSX.Element => {
  const [tugas, setTugas] = useState<Tugas[] | null>(null);
  const { register, handleSubmit } = useForm();

  const fullDate = new Date();
  const year = fullDate.getFullYear();
  let month: number | string = fullDate.getMonth() + 1;
  month = month > 9 ? month : '0' + String(month);
  let date: number | string = fullDate.getDate();
  date = date > 9 ? date : '0' + String(date);
  const currentDate = year + '-' + month + '-' + date;

  const delId = id;

  const onSubmit = (data: Tugas): void => {
    let [year, month, date] = data.deadline.split('-');
    const fullDate = date + '-' + month + '-' + year;
    data.deadline = fullDate;
    if (tugas) setTugas([...tugas, data]);
    if (!tugas) setTugas([data]);
  };

  const handleDelete = (deletedId: string): void => {
    onDelete(deletedId);
  };

  return (
    <CardContainer>
      <CardContentContainer>
        <CardTitle>{title}</CardTitle>
        <CardContent>
          <CardLeftContent>Dosen</CardLeftContent>
          <CardRightContent>{dosen}</CardRightContent>
        </CardContent>
        <CardContent>
          <CardLeftContent>Deskripsi</CardLeftContent>
          <CardRightContent>{deskripsi}</CardRightContent>
        </CardContent>
        <CardContent>
          <CardLeftContent>Semester</CardLeftContent>
          <CardRightContent>{semester}</CardRightContent>
        </CardContent>
        <CardContent>
          <CardLeftContent>Tugas</CardLeftContent>
          <CardRightContent>
            {tugas
              ? tugas.map((t: Tugas, index: number) => {
                  return (
                    <React.Fragment key={index}>
                      &#183; {t.tugas} - Deadline: {t.deadline} <br />
                    </React.Fragment>
                  );
                })
              : 'Belum Ada :)'}
          </CardRightContent>{' '}
        </CardContent>
      </CardContentContainer>
      <CardBottom>
        <CardTitleAddTugas>Tambah Tugas</CardTitleAddTugas>
        <CardForm onSubmit={handleSubmit(onSubmit)}>
          <CardInputContainer>
            <div>
              <CardLabel>Nama Tugas</CardLabel>
              <br />
              <CardInput
                type="text"
                name="tugas"
                ref={register({ required: true })}
              />
            </div>
            <div>
              <CardLabel>Deadline</CardLabel>
              <br />
              <CardInput
                type="date"
                name="deadline"
                min={currentDate}
                ref={register({ required: true })}
              />
            </div>
          </CardInputContainer>
          <CardButtonContainer>
            <CardButton
              colorBtn="#7bbacf"
              colorBtnHvr="#5aadad"
              onClick={() => handleDelete(delId)}
            >
              Hapus
            </CardButton>
            <CardButton colorBtn="#9bba47" colorBtnHvr="#7aad15">
              Tambah
            </CardButton>
          </CardButtonContainer>
        </CardForm>
      </CardBottom>
    </CardContainer>
  );
};
