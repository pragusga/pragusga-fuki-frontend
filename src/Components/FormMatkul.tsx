import { FC } from 'react';
import { useForm } from 'react-hook-form';
import toast, { Toaster } from 'react-hot-toast';
import { v4 } from 'uuid';

import {
  FormContainer,
  FormTextArea,
  FormOption,
  FormSubmit,
  FormTitle,
  FormLabel,
  FormInput,
  Form,
} from './StyledComponents';

export interface Data {
  matkul: string;
  dosen: string;
  sks: string;
  deskripsi: string;
  semester: string;
  tahun: string;
  id?: string;
}

interface FormProps {
  getData: (freshData: Data) => void;
}

export const FormMatkulku: FC<FormProps> = ({ getData }): JSX.Element => {
  const { register, handleSubmit } = useForm();

  const onSubmit = (data: Data) => {
    toast.success('Mata kuliah berhasil ditambahkan');
    data.id = v4();
    getData(data);
  };

  return (
    <FormContainer>
      <Toaster position="top-center" reverseOrder={false} />
      <FormTitle>Tambah Mata Kuliah</FormTitle>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <FormLabel>Nama Mata Kuliah</FormLabel>
        <br />
        <FormInput
          type="text"
          name="matkul"
          ref={register({ required: true })}
        />
        <br />
        <FormLabel>Dosen</FormLabel> <br />
        <FormInput
          type="text"
          name="dosen"
          ref={register({ required: true })}
        />
        <br />
        <FormLabel>Jumlah SKS</FormLabel>
        <br />
        <FormInput
          type="number"
          name="sks"
          min={1}
          max={10}
          ref={register({ required: true })}
        />
        <FormLabel>Deskripsi</FormLabel>
        <br />
        <FormTextArea name="deskripsi" ref={register({ required: true })} />
        <FormLabel>Semester</FormLabel>
        <br />
        <FormOption name="semester" ref={register({ required: true })}>
          <option value="Genap">Genap</option>
          <option value="Ganjil">Ganjil</option>
        </FormOption>
        <br />
        <FormLabel>Tahun Ajar</FormLabel>
        <br />
        <FormOption name="tahun" ref={register({ required: true })}>
          <option value="2020/2021">2020/2021</option>
          <option value="2021/2022">2021/2022</option>
          <option value="2022/2023">2022/2023</option>
        </FormOption>
        <FormSubmit>Tambah</FormSubmit>
      </Form>
    </FormContainer>
  );
};
