import styled, { css } from 'styled-components';

// App
export const AppElement = styled.div`
  width: 100%;
  background-color: #f1f3f6;
  box-sizing: border-box;
  padding: 3px 15% 0 15%;
  display: flex;
  justify-content: space-between;
  padding-top: 40px;
  @media only screen and (max-width: 1441px) {
    padding: 38px 10% 0 10%;
  }
  @media only screen and (max-width: 1186px) {
    flex-direction: column;
    align-items: center;
    align-self: center;
    padding: 38px 0 0 0;
  }
`;

export const ListMatkul = styled.div`
  box-sizing: border-box;
  width: 507px;
  height: auto;
  padding: 0 2px 0 2px;
  position: static;
  @media only screen and (max-width: 1186px) {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  @media only screen and (max-width: 335px) {
    padding: 0 10px 0 10px;
  }
`;

// Navbar
export const NavbarElement = styled.div`
  background-color: #7bbacf;
  position: relative;
  display: flex;
  justify-content: space-between;
  font-weight: 700;
  width: 100% !important;
  height: 63px;
`;

export const NavbarTitle = styled.div`
  position: absolute;
  top: 16px;
  left: 8%;
  color: #f1f3f6;
  line-height: 31.25px;
  font-size: 24px;
`;

export const NavbarBtn = styled.div`
  &:before {
    content: '+ Tambah Matkul';
  }
  width: 200px;
  height: 39px;
  border-radius: 100px;
  background-color: #f1f3f6;
  position: absolute;
  top: 12px;
  right: 8%;
  font-size: 18px;
  line-height: 39px;
  text-align: center;
  color: #7bbacf;
  @media only screen and (max-width: 642px) {
    &:before {
      content: '+';
    }
    width: 36px;
    height: 36px;
    border-radius: 50%;
    line-height: 36px;
  }
`;

// FormMatkul
export const FormInputStyled = css`
  width: 100%;
  height: 36px;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid #424242;
  background: #f1f3f6;
  margin-top: 10px;
  margin-bottom: 19px;
  padding-left: 4px;
  @media only screen and (max-width: 468px) {
    height: 27px;
    width: 335px;
  }
  @media only screen and (max-width: 359px) {
    margin-left: 10px;
    width: 95%;
  }
`;

export const FormContainer = styled.div`
  box-sizing: border-box;
  width: 400px;
  padding: 2px;
  @media only screen and (max-width: 468px) {
    width: 335px;
  }
  @media only screen and (max-width: 359px) {
    width: 100%;
  }
`;

export const Form = styled.form`
  width: 100%;
  height: 100%;
`;

export const FormTitle = styled.div`
  font-size: 20px;
  font-weight: 700;
  line-height: 26.04px;
  color: #9bba47;
  margin-bottom: 25px;
  @media only screen and (max-width: 359px) {
    margin-left: 10px;
  }
`;

export const FormLabel = styled.label`
  font-size: 16px;
  font-weight: 400;
  line-height: 20.83px;
  @media only screen and (max-width: 359px) {
    margin-left: 10px;
  }
`;

export const FormInput = styled.input`
  ${FormInputStyled}
`;

export const FormTextArea = styled.textarea`
  ${FormInputStyled}
  height: 111px;
  @media only screen and (max-width: 468px) {
    height: 104px;
  }
`;

export const FormOption = styled.select`
  ${FormInputStyled}
`;

export const FormSubmit = styled.button`
  width: 120px;
  height: 35px;
  border-radius: 5px;
  background-color: #7bbacf;
  color: #f1f3f6;
  font-size: 16px;
  font-weight: 500;
  line-height: 35px;
  margin: 0 50% 0 50%;
  transform: translate(-50%, 0);
`;

// Card

export const CardContainer = styled.div`
  width: 507px;
  box-sizing: border-box;
  height: 442px;
  background-color: rgba(241, 243, 246, 0.1);
  box-shadow: 5px 5px 30px rgba(55, 84, 170, 0.1);
  border-radius: 10px;
  padding: 30px 32px 30px 32px;
  margin-bottom: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media only screen and (max-width: 1186px) {
    margin-top: 20px;
  }
  @media only screen and (max-width: 734px) {
    width: 385px;
  }
  @media only screen and (max-width: 468px) {
    width: 300px;
    height: 341px;
  }
  @media only screen and (max-width: 375px) {
    width: 270px;
    height: 325px;
  }
`;

export const CardTitle = styled.div`
  opacity: 1;
  color: #7bbacf;
  font-weight: bold;
  font-size: 16px;
  line-height: 21px;
`;

export const CardContentContainer = styled.div`
  color: #424242;
  font-size: 14px;
  line-height: 18.23px;
`;

export const CardContent = styled.div`
  display: flex;
  margin-top: 10px;
  gap: 23px;
`;

export const CardLeftContent = styled.div`
  font-weight: 800;
  box-sizing: border-box;
  width: 80px;
`;
export const CardRightContent = styled.div`
  font-weight: 400;
  padding-left: 10px;
  word-break: break-all;
  width: 100%;
`;

export const CardBottom = styled.div``;

export const CardTitleAddTugas = styled.h2`
  color: #9bba47;
  font-weight: 700;
  font-size: 14px;
  line-height: 18.23px;
`;

export const CardForm = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const CardLabel = styled.label`
  font-weight: 400;
  color: #424242;
  line-height: 15.62px;
  font-size: 12px;
`;

export const CardInputContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const CardInput = styled.input`
  width: 211px;
  height: 28px;
  border-radius: 5px;
  border: 1px solid #424242;
  box-sizing: border-box;
  padding-left: 8px;
  @media only screen and (max-width: 734px) {
    width: 180px;
  }
  @media only screen and (max-width: 468px) {
    width: 130px;
  }
`;

export const CardButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 25px;
  gap: 40px;
`;
export const CardButton = styled.button`
  background-color: ${(props: { colorBtn: string; colorBtnHvr: string }) =>
    props.colorBtn};
  width: 94px;
  height: 30px;
  border-radius: 5px;
  color: #f1f3f6;
  font-weight: 700;
  font-size: 12px;
  line-height: 15.62px;
  &:hover {
    cursor: pointer;
    background-color: ${(props: { colorBtn: string; colorBtnHvr: string }) =>
      props.colorBtnHvr};
  }
`;

// Footer
export const FooterContainer = styled.footer`
  height: 40px;
  width: 100%;
  background-color: #7bbacf;
  font-weight: 400;
  font-size: 14px;
  line-height: 40px;
  color: #f1f3f6;
  text-align: center;
`;
