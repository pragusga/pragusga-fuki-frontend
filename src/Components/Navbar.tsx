import { FC } from 'react';

import { NavbarElement, NavbarTitle, NavbarBtn } from './StyledComponents';

export const Navbar: FC = (): JSX.Element => {
  return (
    <NavbarElement>
      <NavbarTitle>Matkulku</NavbarTitle>
      <NavbarBtn />
    </NavbarElement>
  );
};
